package prov1.entity;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonBackReference;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import jakarta.transaction.Transactional;
@Entity
@Table(name="Juego")
//@Transactional
public class Juego implements Serializable {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="JUG_ID")
    private int juegoId;
	
	@Column(name="NOMBRE")
    private String nombre;
    
    @Column(name="EDAD")
    private Integer edad;
    
    @Column(name="PRECIO")
    private Integer precio;
    
    @ManyToMany(cascade=CascadeType.ALL, mappedBy="juegos",fetch = FetchType.EAGER)
    @JsonBackReference
    Set <Jugador> jugadores = new HashSet<Jugador>();

	public Juego() {
		super();
	}

	public Juego(String nombre, Integer edad, Integer precio) {
		super();
		this.nombre = nombre;
		this.edad = edad;
		this.precio = precio;
	}

	public int getJuegoId() {
		return juegoId;
	}

	public void setJuegoId(int juegoId) {
		this.juegoId = juegoId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	public Integer getPrecio() {
		return precio;
	}

	public void setPrecio(Integer precio) {
		this.precio = precio;
	}

	public Set<Jugador> getJugadores() {
		return jugadores;
	}

	public void setJugadores(Set<Jugador> jugadores) {
		this.jugadores = jugadores;
	}

	@Override
	public String toString() {
		return "Juego [juegoId=" + juegoId + ", nombre=" + nombre + ", edad=" + edad + ", precio=" + precio
				+ ", jugadores=" + jugadores + "]";
	}
    
    
}


    
