package prov1.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import prov1.entity.Juego;
import prov1.entity.Jugador;
import prov1.repository.JugadorRepository;
@Service
public class JugadorService {
	@Autowired
	JugadorRepository repositori;
	
	public Jugador findById(Integer id) {
		return repositori.findById(id).orElse(null);
	}
	
	public List<Jugador> findAll() {
		return repositori.findAll();
	}
	
	public void delete(Integer id) {
		
		repositori.deleteById(id);  
	}
	public void deleteAll() {

		repositori.deleteAll();
	}
	public Jugador insertar(Jugador j) {
		return repositori.save(j);
	}
	public Jugador editar(Jugador j) {
		return repositori.save(j);
	}
	
	public List<Jugador> jugadorConSaldo(){
		return repositori.findBySaldoGreaterThanEqual(500);
	}
	
	public List<Jugador> jugadorConSaldo2(int saldo){
		return repositori.findJugadorConSaldoQuery(saldo);
	}
	
	public List<Jugador> jugadorConSaldo3(int saldo){
		return repositori.findBySaldoGreaterThanEqual(saldo);
	}
	
	public List<Jugador> findByJuego(Juego juego){
	
		return repositori.findByJuegos(juego);
	}
	
	public List<Jugador> findByApellidosIgnoreCase(String ape1){
		return repositori.findByApellidosIgnoreCase(ape1);
	}
	
	public List<Jugador> findByNombreAndApellidosAllIgnoreCase(String nombre, String apellidos){
		return repositori.findByNombreAndApellidosAllIgnoreCase(nombre, apellidos);

	}
	
	public List<Jugador> findByApellidosOrderByNombreAsc(String apellidos){
		return repositori.findByApellidosOrderByNombreAsc(apellidos);
	}
	
	public List<Jugador> findByApellidosOrderByNombreDesc(String apellidos){
		return repositori.findByApellidosOrderByNombreDesc(apellidos);
	}
	
}
