package prov1.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import prov1.entity.Juego;
import prov1.repository.JuegoRepository;
@Service
public class JuegoService {
	@Autowired
	JuegoRepository repositori;

	public Juego findById(Integer id) {
		return repositori.findById(id).orElse(null);
	}

	public List<Juego> findAll() {
		return repositori.findAll();
	}

	public void delete(Integer id) {

		repositori.deleteById(id);
	}
	
	public void deleteAll() {

		repositori.deleteAll();
	}

	public Juego insertar(Juego j) {
		return repositori.save(j);
	}

	public Juego editar(Juego j) {
		return repositori.save(j);
	}
	
}

