package prov1.utils;

import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GestioExcepcions extends ResponseEntityExceptionHandler {
	
	@ExceptionHandler(Exception.class)
	protected ResponseEntity<?> handleGenericException(Exception ex, WebRequest request){
		RespostaError respuestaError = new RespostaError(ex.getMessage());
		return handleExceptionInternal(ex, respuestaError, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
	}
	
	// ************************************************************************************************************
	
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		
		// TODO modificar mensaje standard en ingles....
		RespostaError respuestaError = new RespostaError(ex.getMessage());
		return handleExceptionInternal(ex, respuestaError, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
	}
	
	// ************************************************************************************************************

	@ExceptionHandler(PresentacioException.class)
	protected ResponseEntity<?> handlePresentationException(PresentacioException ex, WebRequest request){
		RespostaError respuestaError = new RespostaError(ex.getMessage());
		return handleExceptionInternal(ex, respuestaError, new HttpHeaders(), ex.getHttpStatus(), request);
	}
	
	// ************************************************************************************************************

	protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		
		Object valor = ex.getValue();
		String tipusEntrant = valor.getClass().getName();
		String tipusRequerit = (ex.getRequiredType()).getName();
		
		RespostaError respuestaError = new RespostaError("El paràmetre " + valor + " és de tipus " + tipusEntrant + ". No es pot parsejar a " + tipusRequerit);
		return handleExceptionInternal(ex, respuestaError, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
	}
	
	// ************************************************************************************************************
	
	

}
