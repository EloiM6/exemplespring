package prov1.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import prov1.entity.Juego;

public interface JuegoRepository extends JpaRepository<Juego, Integer>{

}
