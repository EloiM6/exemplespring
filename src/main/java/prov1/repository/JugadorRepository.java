package prov1.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import prov1.entity.Juego;
import prov1.entity.Jugador;

public interface JugadorRepository extends JpaRepository<Jugador, Integer>{
	List<Jugador> findByJuegos(Juego juego);
	
	List<Jugador> findByApellidosIgnoreCase(String ape1);
	List<Jugador> findByNombreAndApellidosAllIgnoreCase(String nombre, String apellidos);

	List<Jugador> findByApellidosOrderByNombreAsc(String apellidos);
	List<Jugador> findByApellidosOrderByNombreDesc(String apellidos);
	
	
	List<Jugador>findBySaldoGreaterThanEqual(int saldo);
	
	@Query("Select j from Jugador j where j.saldo >= :saldo")
	List<Jugador> findJugadorConSaldoQuery(@Param("saldo") int saldo);
}
