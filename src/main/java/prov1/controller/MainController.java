package prov1.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import prov1.entity.Juego;
import prov1.entity.Jugador;
import prov1.services.JuegoService;
import prov1.services.JugadorService;
import prov1.utils.PresentacioException;
import prov1.utils.ReturnOfJedy;

// URL para consultar apis http://localhost:9000/swagger-ui/index.html/

@RestController
//@Controller

//@RequestMapping(path="/vacio")
public class MainController {

	@Autowired
	JuegoService juegoService;
	
	@Autowired
	JugadorService jugadorService;
	
	/*@GetMapping(path="/")
	public @ResponseBody String welcome() {
		return "Hello";
	}*/
	@GetMapping(path="/")
	public ResponseEntity<?> welcome() {
		
		return ResponseEntity.ok(new ReturnOfJedy("Hello")) ;
	}
	
	@GetMapping(path="/Prova1")
	public String hola() {
		return "prova1";
	}
	/*
	@GetMapping(path="/allJugadors")
	public @ResponseBody List<Jugador> allJugadors() {
		return jugadorService.findAll();
	}*/
	@GetMapping(path="/allJugadors")
	public ResponseEntity<?> allJugadors() {
		List<Jugador> jugadors = jugadorService.findAll();
		return ResponseEntity.ok(jugadors);
	}
	@GetMapping(path="/Jugador/add")
	public @ResponseBody String addJugador(@RequestParam String name, @RequestParam String ape, @RequestParam int saldo) {
		Jugador j = new Jugador();
		j.setNombre(name);
		j.setApellidos(ape);
		j.setSaldo(saldo);
		
		jugadorService.insertar(j);
		
		return "Guardat!!";
	}
	
	@GetMapping(path="/Jugador/add2")
	public ResponseEntity<?> addJugador2(@RequestParam String name, @RequestParam String ape, @RequestParam int saldo) {
		Jugador j = new Jugador();
		j.setNombre(name);
		j.setApellidos(ape);
		j.setSaldo(saldo);
		
		jugadorService.insertar(j);
		
		return ResponseEntity.ok(new ReturnOfJedy("Guardat!!!")) ;
	}
	
	@PostMapping(path="/Jugador/add2")
	public ResponseEntity<?> addJugador3(@RequestParam String name, @RequestParam String ape, @RequestParam int saldo) {
		Jugador j = new Jugador();
		j.setNombre(name);
		j.setApellidos(ape);
		j.setSaldo(saldo);
		
		jugadorService.insertar(j);
		
		return ResponseEntity.ok(new ReturnOfJedy("Guardat!!!")) ;
	}
	
	@PostMapping(path="/Jugador/add3")
	public ResponseEntity<?> addJugador4(Jugador j) {
		
		jugadorService.insertar(j);
		
		return ResponseEntity.ok(new ReturnOfJedy("Guardat!!!")) ;
	}
	
	
	@GetMapping(path = "/allJuegos")
	public @ResponseBody List<Juego> getAllJuegos() {
		// This returns a JSON or XML with the users
		return juegoService.findAll();
	}
	@GetMapping(path = "/Jugador/addJuego") // Map ONLY GET Requests
	public @ResponseBody String addJugador(@RequestParam int idJugador, @RequestParam int idJuego) {
		// @ResponseBody means the returned String is the response, not a view name
		// @RequestParam means it is a parameter from the GET or POST request

		Jugador j = jugadorService.findById(idJugador) ;
		Juego juego = juegoService.findById(idJuego) ;
		j.getJuegos().add(juego);
		jugadorService.editar(j);
		return "Saved";
	}
	
	@GetMapping(path = "/Juego/addJugador") // Map ONLY GET Requests
	public @ResponseBody String addJuego(@RequestParam int idJuego, @RequestParam int idJugador) {
		// @ResponseBody means the returned String is the response, not a view name
		// @RequestParam means it is a parameter from the GET or POST request

		Jugador j = jugadorService.findById(idJugador) ;
		Juego juego = juegoService.findById(idJuego) ;
		juego.getJugadores().add(j);
		juegoService.editar(juego);
		return "Saved";
	}
	
	@GetMapping(path = "/Jugador/edit") // Map ONLY GET Requests
	public @ResponseBody String editJugador(@RequestParam String name, @RequestParam String apellidos,
			@RequestParam int saldo) {
		// @ResponseBody means the returned String is the response, not a view name
		// @RequestParam means it is a parameter from the GET or POST request

		Jugador j = new Jugador();
		j.setNombre(name);
		j.setApellidos(apellidos); 
		j.setSaldo(saldo);
		jugadorService.editar(j);
		return "Saved";
	}
	
	@PutMapping(path = "/Jugador/edit")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public @ResponseBody void editJugador2(Jugador j) {
		
		if (jugadorService.findById(j.getJugId() ) != null) {
			
		
			jugadorService.editar(j);
		} else {
			throw new PresentacioException("No existeix aquest jugador amb id " + j.getJugId(), HttpStatus.NOT_FOUND);
		}

		//return "Saved";
	}
	
	@GetMapping(path = "/Juego/add") // Map ONLY GET Requests
	public @ResponseBody String addJuego(@RequestParam String name, @RequestParam int edat,
			@RequestParam int precio) {
		
		Juego j = new Juego();
		j.setNombre(name);
		j.setEdad(edat); 
		j.setPrecio(precio);
		juegoService.editar(j);
		return "Saved";
	}
	
	@GetMapping(path = "/Juego/edit") // Map ONLY GET Requests
	public @ResponseBody String editJuego(@RequestParam String name, @RequestParam int edat,
			@RequestParam int precio) {
		
		Juego j = new Juego();
		j.setNombre(name);
		j.setEdad(edat); 
		j.setPrecio(precio);
		juegoService.editar(j);
		return "Saved";
	}
	
	@GetMapping(path = "/jugadorDelete/{id}")
	public @ResponseBody String deleteJugador(@PathVariable Integer id) {
		jugadorService.delete(id) ;
		return "Borrat";
	}
	@DeleteMapping(path = "/jugadorDelete/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)

	public @ResponseBody void deleteJugador2(@PathVariable Integer id) {
		jugadorService.delete(id) ;
		//return "Borrat";
	}
	@GetMapping(path = "/juegoDelete/{id}")
	public @ResponseBody String deleteJuego(@PathVariable Integer id) {
		juegoService.delete(id) ;
		return "Borrat";
	}
	
	@GetMapping(path = "/juego/{id}")
	public @ResponseBody Juego getJuego(@PathVariable int id) {
		Juego j = juegoService.findById(id) ;
		if(j==null) {
			System.out.println("No es troba aquest ID " + id);
			throw new PresentacioException("No es troba el joc amb id " + id, HttpStatus.NOT_FOUND);
		}
		return j;
	}
	@GetMapping(path = "/JugadorConPasta")
	public @ResponseBody List<Jugador> getJugadoresConPasta() {
		// This returns a JSON or XML with the users
		return jugadorService.jugadorConSaldo();
	}
	
	@GetMapping(path = "/JugadorConPasta2")
	public @ResponseBody List<Jugador> getJugadoresConPasta2(@RequestParam int saldo) {
		// This returns a JSON or XML with the users
		return jugadorService.jugadorConSaldo2(saldo);
	}
	
	// Exemple d'element opcional  (name="q", required=false)
	
	@GetMapping(path = "/JugadorConPasta3")
	public @ResponseBody List<Jugador> getJugadoresConPasta3(@RequestParam (name="saldo", required=false) String saldo) {
		// This returns a JSON or XML with the users
		List<Jugador> resultado = (saldo==null)? jugadorService.findAll() : jugadorService.jugadorConSaldo3(Integer.parseInt(saldo));
		return resultado;
	}
	
	
	@GetMapping(path = "/JugadoresJuego/{id}")
	public @ResponseBody List<Jugador> findByJuego(@PathVariable Integer id){
		
		Juego juego = juegoService.findById(id);
		
		return jugadorService.findByJuego(juego);
	}
	
	@GetMapping(path = "/JugadorApellido")
	public @ResponseBody List<Jugador> findByApellidosIgnoreCase(@RequestParam String ape1){
		return jugadorService.findByApellidosIgnoreCase(ape1);
	}
	
	@GetMapping(path = "/JugadorNombreApellido")
	public @ResponseBody List<Jugador> findByNombreAndApellidosIgnoreCase(@RequestParam String nombre, @RequestParam String apellidos){
		return jugadorService.findByNombreAndApellidosAllIgnoreCase(nombre, apellidos);

	}
	
	@GetMapping(path = "/JugadorNombreApellidosAsc")
	public @ResponseBody List<Jugador> findByApellidosOrderByNombreAsc(@RequestParam String apellidos){
		return jugadorService.findByApellidosOrderByNombreAsc(apellidos);
	}
	
	
	@GetMapping(path = "/JugadorNombreApellidosDesc")
	public @ResponseBody List<Jugador> findByApellidosOrderByNombreDesc(@RequestParam String apellidos){
		return jugadorService.findByApellidosOrderByNombreDesc(apellidos);
	}
	
}
